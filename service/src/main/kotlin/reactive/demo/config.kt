package reactive.demo

import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.http.client.reactive.ReactorResourceFactory
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import org.springframework.web.reactive.function.client.WebClient
import reactor.netty.http.client.HttpClient

@Configuration
open class Config {

    @Bean
    open fun resourceFactory(): ReactorResourceFactory {
        val factory = ReactorResourceFactory()
        factory.isUseGlobalResources = false
        return factory
    }

    @Bean
    open fun webClient(): WebClient.Builder {

        val mapper = { client: HttpClient -> client }

        val connector = ReactorClientHttpConnector(resourceFactory(), mapper)

        return WebClient.builder().clientConnector(connector)
    }

    @Bean
    open fun objectMapperBuilder(): Jackson2ObjectMapperBuilder =
        Jackson2ObjectMapperBuilder().modulesToInstall(KotlinModule())
}