package reactive.demo

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.web.reactive.HttpHandlerAutoConfiguration
import org.springframework.boot.autoconfigure.web.reactive.ReactiveWebServerFactoryAutoConfiguration
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.web.reactive.config.DelegatingWebFluxConfiguration


@Configuration
@ComponentScan
@Import(value = [
    DelegatingWebFluxConfiguration::class,
    ReactiveWebServerFactoryAutoConfiguration::class,
    HttpHandlerAutoConfiguration::class
])
open class Application

fun main(args: Array<String>) {
    val app = SpringApplication(Application::class.java)
    //app.setLogStartupInfo(false)
    app.run(*args)
}