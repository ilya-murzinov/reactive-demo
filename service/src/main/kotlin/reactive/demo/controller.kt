package reactive.demo

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import reactor.core.scheduler.Scheduler
import reactor.core.scheduler.Schedulers

@RestController
@RequestMapping(path = ["/api/digest"])
class UserController(@Autowired val webClient: WebClient.Builder) {

    private val s: Scheduler = Schedulers.single()

    private val client = webClient.baseUrl("https://www.revolut.com/api/").build()

    private val lastTxns = client
        .get()
        .uri("transactions/last")
        .accept(MediaType.APPLICATION_JSON)
        .retrieve()
        .bodyToFlux(Transaction::class.java)
        .collectList()

    private val users = client
        .get()
        .uri("users/count")
        .accept(MediaType.APPLICATION_JSON)
        .retrieve()
        .bodyToMono(UserCount::class.java)

    private val allTransactions = Mono.zip(lastTxns, lastTxns, lastTxns).map { it.t1 + it.t2 + it.t3 }

    @GetMapping("/")
    fun digest(): Mono<Response> = allTransactions
        .zipWith(users)
        .publishOn(s)
        .map { tuple -> Response(tuple.t2.count, tuple.t1) }

    @GetMapping("/dummy")
    fun dummy(): Mono<String> = Mono.just("Ok")
}

data class Response(val users: Int, val txns: List<Transaction>)