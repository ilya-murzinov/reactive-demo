package reactive.demo

data class Transaction(
    val type: String,
    val city: String,
    val amount: Amount
) {
    data class Amount(
        val ccy: String,
        val value: Int
    )
}

data class UserCount(val count: Int)